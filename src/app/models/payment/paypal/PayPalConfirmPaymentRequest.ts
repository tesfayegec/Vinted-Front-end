export class PayPalConfirmPaymentRequest{
    paymentId: string;
    payerId: string;
    userID:number;
    productId: string;
  }
  