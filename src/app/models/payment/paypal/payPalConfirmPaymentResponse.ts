export class PayPalConfirmPaymentResponse{
    status: string;
    paidPrice:number;
    paymentID: string;
    PaymentDate: string;
    payeeName: string;
    paymentMode: string;
    payerName: string;
    addressLine1: string;
    city: string;
    countryCode: string;
    postalCode: string;
    state: string;
    payerEmail: string;
    payer_id: string;
   
    
  }
  