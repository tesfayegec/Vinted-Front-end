import { CallsComponent } from './components/calls/calls.component';
import { CallRequest } from './models/callRequest.model';
import { SearchUsersComponent } from './components/search-users/search-users.component';
import { UserComponent } from './components/user/user.component';
import { PersonalizationGuard } from './guard/Personalization.guard';
import { AdminGuard } from './guard/admin.guard';
import { ProductModelServer } from './models/product.model';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductComponent} from './components/product/product.component';
import {CartComponent} from './components/cart/cart.component';
import {CheckoutComponent} from './components/checkout/checkout.component';
import {ThankyouComponent} from './components/thankyou/thankyou.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {ProfileGuard} from './guard/profile.guard';
import {RegisterComponent} from './components/register/register.component';
import {HomeComponent} from './components/home/home.component';
import {HomeLayoutComponent} from './components/home-layout/home-layout.component';
import {ContactComponent} from './components/contact/contact.component';
import { CategoryComponent } from './components/category/category.component';

import { PersonalizationComponent } from './components/personalization/personalization.component';
import { CreateProductComponent } from './components/sell-area/create-product/create-product.component';
import { EditProductComponent } from './components/sell-area/edit-product/edit-product.component';
import { MessageComponent } from './components/message/message.component';
import { PurchaseComponent } from './components/purchase/purchase.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { CardComponent } from './components/card/card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PaypalComponent } from './components/payment/paypal/paypal.component';
import { PaymentSuccessComponent } from './components/payment/payment-success/payment-success.component';
import { MyproductsComponent } from './components/myproducts/myproducts.component';
import { InoutComponent} from './components/message/inout/inout.component';
import { UsersproductsComponent } from './components/usersproducts/usersproducts.component';
import { SearchProductComponent } from './components/search-product/search-product.component';
import { PaymenthistoryComponent } from './components/paymenthistory/paymenthistory.component';

const routes: Routes = [
  // Define routes for the landing / home page, create a separate component for the layout of home page
  // put only header, footer and router-outlet there
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path: '', component: HomeComponent

      },
      {
        path: 'product/:id', component: ProductComponent
      },
      {
        path: 'cart', component: CartComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'checkout', component: CheckoutComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'thankyou', component: ThankyouComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'login', component: LoginComponent
      },

      {
        path: 'profile', component: ProfileComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'user/:username', component: UserComponent
      },
      {
        path: 'searchUser/:searchText', component: SearchUsersComponent
      },
      {
        path: 'searchProduct/:searchText', component: SearchProductComponent
      },
      {
        path: 'register', component: RegisterComponent
      },
      {
        path: 'personalization', component: PersonalizationComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'contact', component: ContactComponent
      },
      {
        path: 'clothes' , component: CategoryComponent
      },
      {
        path: 'shoes' , component: CategoryComponent
      },
      {
        path: 'accessories', component: CategoryComponent
      },
      {
        path: 'cook' , component: CategoryComponent
      },
      {
        path: 'technology' , component: CategoryComponent
      },
      {
        path: 'book' , component: CategoryComponent
      },
      {
        path: 'product', component: ProductComponent
      },
      {
        path: 'favorite', component: FavoriteComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'create-product', component: CreateProductComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'edit-product/:id', component: EditProductComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'message', component: InoutComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'calls', component: CallsComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'purchase/:id', component: PurchaseComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'addCard', component: CardComponent
        , canActivate: [ProfileGuard]
      },
      {
        path: 'myproducts', component: MyproductsComponent,
        canActivate: [ProfileGuard]
      },
      {
        path: 'usersproducts/:id', component: UsersproductsComponent ,
        canActivate: [ProfileGuard]
      },
      {
        path: 'paymenthistory', component: PaymenthistoryComponent,
        canActivate: [ProfileGuard]
      }
    ]
  },
  // Wildcard Route if no route is found == 404 NOTFOUND page
   // Wildcard Route if no route is found == 404 NOTFOUND page
   { path: 'myprofile', loadChildren: () => import('./components/profile/profile/profile.module').then(m => m.ProfileModule) },

   {
    path: 'payment', component: PaypalComponent
    // , canActivate: [ProfileGuard, PersonalizationGuard]
  },

  {
    path: 'paymentsuccess', component: PaymentSuccessComponent
    // , canActivate: [ProfileGuard, PersonalizationGuard]
  },

  // { path: 'myprofile', loadChildren: () => import(`./profile/profile.module`).then(m => m.ProfileModule)
   {
    path: '**', pathMatch: 'full', redirectTo: ''
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes),FormsModule,ReactiveFormsModule,CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
